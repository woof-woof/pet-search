from ddf import G  # type: ignore
from petsearch.core.models.bid import LostBid, Pet, Place

for _ in range(1000):
    lost_pet = G(Pet)
    lost_pet.save()
    lost_bid = G(LostBid, pet=lost_pet)
    lost_bid.save()
    lost_place = G(Place)
    lost_place.save()
