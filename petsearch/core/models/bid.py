from django.db import models

from petsearch.core.models.pet import Pet
from petsearch.core.models.place import Place


class Bid(models.Model):
    timestamp = models.fields.DateTimeField(auto_now=True)
    pet = models.OneToOneField(Pet, on_delete=models.CASCADE, primary_key=True)
    places = models.ManyToManyField(Place, blank=True)

    class Meta:
        abstract = True


class LostBid(Bid):
    pass


class FoundBid(Bid):
    pass
