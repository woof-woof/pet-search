from django.db import models


class Pet(models.Model):
    name = models.CharField(max_length=128)


class PetImage(models.Model):
    pet = models.ForeignKey(Pet, related_name='images', on_delete=models.CASCADE)
    image = models.ImageField()
