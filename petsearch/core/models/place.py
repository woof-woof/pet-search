from django.contrib.gis.db import models as gis_models


class Place(gis_models.Model):
    label = gis_models.CharField(max_length=128)
