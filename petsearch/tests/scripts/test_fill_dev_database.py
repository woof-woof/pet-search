import pytest

from petsearch.core.models.bid import LostBid
from petsearch.core.models.pet import Pet
from petsearch.core.models.place import Place


@pytest.mark.django_db
def test_fill_dev_database():
    """Проверка petsearch.scripts.fill_dev_database"""
    # импортируем файл
    import petsearch.scripts.fill_dev_database # pylint: disable=unused-import, import-outside-toplevel # noqa
    # а потом чекаем что нужные объекты создались или чо там скрипт делает
    assert LostBid.objects.count() == 1000
    assert Pet.objects.count() == 1000
    assert Place.objects.count() == 1000
