from rest_framework import serializers
from petsearch.core.models.bid import LostBid


class LostBidSerializer(serializers.ModelSerializer):
    class Meta:
        model = LostBid
        fields = ('timestamp',)
