from rest_framework.routers import DefaultRouter

from .views import LostBidsViewSet


router = DefaultRouter()
router.register(r'lost_bids', LostBidsViewSet, basename='lost_bids')
urlpatterns = router.urls
