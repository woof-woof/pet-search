from rest_framework import viewsets

from petsearch.core.models.bid import LostBid
from .serializers import LostBidSerializer


class LostBidsViewSet(viewsets.ModelViewSet):
    serializer_class = LostBidSerializer
    queryset = LostBid.objects.all()
