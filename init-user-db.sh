#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE USER petsearch WITH LOGIN SUPERUSER PASSWORD 'petsearch';
    CREATE DATABASE petsearch;
    GRANT ALL PRIVILEGES ON DATABASE petsearch TO petsearch;
EOSQL